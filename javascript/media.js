
/**
 *  @file
 *  This file handles the JS for Media Module functions.
 */

(function ($) {

/**
 * Loads media browsers and callbacks, specifically for media as a field.
 */
Drupal.behaviors.mediaElement = {
  attach: function (context, settings) {
    // Options set from media.fields.inc for the types, etc to show in the browser.
    
    // For each widget (in case of multi-entry)
    $('.media-widget', context).once('mediaBrowserLaunch', function () {
      var options = settings.media.elements[this.id];
      globalOptions = {};
      if (options.global != undefined) {
        var globalOptions = options.global;
      }
      //options = Drupal.settings.media.fields[this.id];
      var fidField = $('.fid', this);
      var previewField = $('.preview', this);
      var removeButton = $('.remove', this); // Actually a link, but looks like a button.

      // Show the Remove button if there's an already selected media.
      if (fidField.val() != 0) {
        removeButton.css('display', 'inline-block');
      }
      
      // Test if part of multiple field
      var parents = $(this).parents('.field-multiple-table');
      var multiselect = false;
      if (parents.size() > 0) {
        var parent = parents.get(0);
        multiselect = true;
        var brothers = $('.media-widget', parent);
        globalOptions.multiselect = 1;
      }

      // When someone clicks the link to pick media (or clicks on an existing thumbnail)
      $('.launcher', this).bind('click', function () {
        // Launch the browser, providing the following callback function
        // @TODO: This should not be an anomyous function.
        Drupal.media.popups.mediaBrowser(function (mediaFiles) {
          if (mediaFiles.length < 0) {
            return;
          }
          var mediaFile = mediaFiles[0];
          // Set the value of the filefield fid (hidden).
          fidField.val(mediaFile.fid);
          // Set the preview field HTML.
          previewField.html(mediaFile.preview);
          // Show the Remove button.
          removeButton.show();
          
          // Take care for other items if multiple field
          if (multiselect) {
            // Remove empty widgets
            $('tbody tr', parent).each(function (index, elem) {
              var alert = $('input.fid', elem).val();
              if ($('input.fid', elem).val() == 0) {
                $(elem).remove();
              }
            });
            
            // Add other items from set
            // TODO: find way to push items to field widgets
            /* First i tried to clone one item and populate all values/ids. It 
             * looks that Drupal needs to know about every new field item, which
             * means we need to make an AJAX call for every single item we want
             * to create. This can be done via .trigger(), but the problem is,
             * that it executes async and we are unable to populate new field 
             * after it was created.
             *
             * http://drupal.org/node/951004#comment-4408846
            */
            for (var i = 1; i < mediaFiles.length; i++) {
              $('.field-add-more-submit', $(parent).parent()).trigger('mousedown');
              /*var n = $('tbody tr', parent).last();
              $('.media-widget .fid', n).val(mediaFiles[i].fid);
              $('.media-widget .preview', n).html(mediaFiles[i].preview);
              $('.media-widget .remove', n).show();*/
            }
            
            
            // Correct ids, weights, odd/even, ...
            /*var images = $('tbody tr', parent);
            for (var i=0; i < images.size(); i++) {
              // Load current table row (multimedia asset)
              var item = $(images.get(i));
              
              // Set odd/even
              if (i % 2 == 0) {
                item.removeClass('odd').removeClass('even').addClass('odd');
              }
              else {
                item.removeClass('odd').removeClass('even').addClass('even');
              }
              
              // Correct widget ID
              var id = $('.media-widget', item).attr('id').split('-');
              id[id.length-1] = i;
              $('.media-widget', item).attr('id', id.join('-'));
              $('.media-widget div.form-type-item', item).attr('id', id.join('-'));

              // Correct fid form item
              var fid = $('.media-widget .form-type-textfield', item);
              var name = $('input', fid).attr('name').split('[');
              name[2] = i + ']';
              $('input', fid).attr('name', name.join('['));
              
              id = $('input', fid).attr('id').split('-');
              var old_id = id.slice(0);
              id[id.length-2] = i;
              $('input', fid).attr('id', id.join('-'));
              
              old_id = old_id.slice(1);
              id = id.slice(1);
              
              $(fid).removeClass('form-item-' + old_id.join('-'));
              $(fid).addClass('form-item-' + id.join('-'));
              
              // Correct weights
              old_id = old_id.slice(0,old_id.length-1);
              id = id.slice(0,id.length-1);
              
              $('.delta-order .form-item', item).removeClass('form-item-' + old_id.join('-') + '--weight').addClass('form-item-' + id.join('-') + '--weight');
              $('.delta-order label', item).attr('for', 'edit-' + id.join('-') + '-weight');
              $('.delta-order select', item).attr('id', 'edit-' + id.join('-') + '-weight');
              
              name = $('.delta-order select', item).attr('name').split('[');
              name[2] = i + ']';
              $('.delta-order select', item).attr('name', name.join('['));
              
              
              
              
            }*/
          }
          
        }, globalOptions);
        return false;
      });

      // When someone clicks the Remove button.
      $('.remove', this).bind('click', function () {
        // Set the value of the filefield fid (hidden).
        fidField.val(0);
        // Set the preview field HTML.
        previewField.html('');
        // Hide the Remove button.
        removeButton.hide();
        return false;
      });

      $('.media-edit-link', this).bind('click', function () {
        var fid = fidField.val();
        if (fid) {
          Drupal.media.popups.mediaFieldEditor(fid, function (r) { alert(r); });
        }
        return false;
      });

    });
  }
};

})(jQuery);
